<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Repository\InMemoryArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    /**
     * @Route("/articles")
     * @Method("POST")
     */
    public function getArticles()
    {
        $repository = new InMemoryArticleRepository();

        if ($repository) {
            $articles = $repository->findAll();
            $articlesData = array();

            foreach ($articles as $data) {
                $articlesData[] = [
                    'id' => $data->getId(), 
                    'name' => $data->getName(), 
                    'description' => $data->getDescription(), 
                    'img' => $data->getImg()
                ];
            }

            return new JsonResponse($articlesData);
        }
        return new Response('not exist');
    }
    
}






const ArticleView = {
    name: 'article-view',
    template: `
    <div class="wrapper">
        <div 
            class="back clickable"
            @click="$router.push('/')"
        >
           < Back
        </div>
        <section 
            class="article-container"
            v-if="article"
        >
            <img :src="article.img"/>
            <div class="article-data">
                <h1>{{ article.name }}</h1>
                <p>{{ article.description }}</p>
                <div class="article-button">
                    NOT AVALIABLE
                </div>
            </div>
        </section>
    </div>
    `,
    data: () => {
        return {
            title: 'Title test',
            articlesLoaded: false
        }
    },
    computed: {
        articles() {
            return this.$store.state.articles || [];
        },
        article() {
            if (!this.articles.length) { return; }
            return this.articles.find(_ => _.id == this.$route.params.id) || '';
        }
    }
}

export default ArticleView;
const HomeView = {
    name: 'home-view',
    template: `
    <div class="wrapper">
        <section class="banner-container">
            <div class="banner-content">
                <h1>Lorem Ipsum</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuasantium doloremque laudatinum, titam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>
                <div class="banner-button clickable">
                    LOREM IPSUM
                </div>
            </div>
        </section>
        <section 
            class="articles-container" 
            :class="{visible: hasArticles}"
        >
            <div class="articles-content">
                <p>{{ articlesLength }} products</p>
                <div class="cards-container">
                    <div 
                        class="card"
                        v-for="article in articles"
                        :key="article.id"
                    >
                        <img :src="article.img"/>
                        <p>{{ article.description }}</p>
                        <div 
                            class="card-button clickable"
                            @click="goto(article.id)"
                        >
                            BUY
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    `,
    computed: {
        articles() {
            return this.$store.state.articles || {};
        },
        articlesLength() {
            return this.articles.length || 0;
        },
        hasArticles() {
            return this.$store.state.haveArticles;
        }
    },
    methods: {
        goto(id) {
            this.$router.push(`article/${id}/buy`);
        }
    }
}

export default HomeView;
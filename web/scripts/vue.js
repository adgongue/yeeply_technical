import router from './router.js';
import store from './store.js';


Vue.config.devtools = true;

new Vue({
    router,
    store,
    el: '#app',
    data: function() {
        return {
            open: false
        }
    },
    mounted() {
        this.$store.dispatch('getArticles');
    }
});


import HomeView from './vue-views/HomeView.js';
import ArticleView from './vue-views/ArticleView.js';

const routes = [
    { 
        path: '/', 
        component: HomeView 
    },
    {
        path: '/article/:id/buy',
        component: ArticleView
    },
];

const router = new VueRouter({
    routes,
});

export default router;


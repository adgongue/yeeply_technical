const store = new Vuex.Store({
    state: {
        articles: {},
        haveArticles: false
    },
    mutations: {
        setArticles(state, value) {
            state.articles = value;
        },
        setHaveArticles(state, value) {
            state.haveArticles = value;
        }
    },
    actions: {
        getArticles(context) {
            let data;
            new Promise((res) => {
                $.ajax({
                    type: 'POST',
                    url: '/articles',
                    dataType: 'json',
                    cache: false,
                    success: function (result) {
                        res(result);
                    },
                });
            }).then((data) => {
                context.commit('setArticles', data);
                context.commit('setHaveArticles', true);
            });
        }
    }
});

export default store;
